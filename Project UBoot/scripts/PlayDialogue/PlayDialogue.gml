function PlayDialogue(argument0)
{
	if(instance_exists(oDialogueSystem))
	{
		show_debug_message("You can't start two dialogues at once");
	}
	else
	{
		with(instance_create_depth(0,0,0, oDialogueSystem))
		{
			dialogue = argument0;
			event_user(0);
		}
		
	}
}
{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "ELEGANT TYPEWRITER",
  "styleName": "Regular",
  "size": 13.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":6,"h":20,"character":32,"shift":6,"offset":0,},
    "33": {"x":71,"y":90,"w":3,"h":20,"character":33,"shift":5,"offset":1,},
    "34": {"x":76,"y":90,"w":6,"h":20,"character":34,"shift":6,"offset":0,},
    "35": {"x":84,"y":90,"w":9,"h":20,"character":35,"shift":9,"offset":-1,},
    "36": {"x":95,"y":90,"w":9,"h":20,"character":36,"shift":9,"offset":0,},
    "37": {"x":106,"y":90,"w":8,"h":20,"character":37,"shift":10,"offset":1,},
    "38": {"x":116,"y":90,"w":9,"h":20,"character":38,"shift":10,"offset":0,},
    "39": {"x":127,"y":90,"w":3,"h":20,"character":39,"shift":3,"offset":0,},
    "40": {"x":132,"y":90,"w":3,"h":20,"character":40,"shift":5,"offset":1,},
    "41": {"x":137,"y":90,"w":3,"h":20,"character":41,"shift":4,"offset":0,},
    "42": {"x":142,"y":90,"w":8,"h":20,"character":42,"shift":9,"offset":0,},
    "43": {"x":152,"y":90,"w":7,"h":20,"character":43,"shift":8,"offset":0,},
    "44": {"x":161,"y":90,"w":3,"h":20,"character":44,"shift":4,"offset":0,},
    "45": {"x":166,"y":90,"w":8,"h":20,"character":45,"shift":8,"offset":0,},
    "46": {"x":176,"y":90,"w":3,"h":20,"character":46,"shift":4,"offset":0,},
    "47": {"x":181,"y":90,"w":7,"h":20,"character":47,"shift":7,"offset":0,},
    "48": {"x":190,"y":90,"w":7,"h":20,"character":48,"shift":7,"offset":0,},
    "49": {"x":199,"y":90,"w":3,"h":20,"character":49,"shift":4,"offset":0,},
    "50": {"x":63,"y":90,"w":6,"h":20,"character":50,"shift":8,"offset":1,},
    "51": {"x":204,"y":90,"w":6,"h":20,"character":51,"shift":7,"offset":0,},
    "52": {"x":54,"y":90,"w":7,"h":20,"character":52,"shift":8,"offset":0,},
    "53": {"x":33,"y":90,"w":8,"h":20,"character":53,"shift":7,"offset":-1,},
    "54": {"x":104,"y":68,"w":7,"h":20,"character":54,"shift":7,"offset":0,},
    "55": {"x":113,"y":68,"w":7,"h":20,"character":55,"shift":8,"offset":0,},
    "56": {"x":122,"y":68,"w":7,"h":20,"character":56,"shift":7,"offset":-1,},
    "57": {"x":131,"y":68,"w":7,"h":20,"character":57,"shift":7,"offset":0,},
    "58": {"x":140,"y":68,"w":3,"h":20,"character":58,"shift":4,"offset":0,},
    "59": {"x":145,"y":68,"w":4,"h":20,"character":59,"shift":3,"offset":-1,},
    "60": {"x":151,"y":68,"w":12,"h":20,"character":60,"shift":11,"offset":-1,},
    "61": {"x":165,"y":68,"w":9,"h":20,"character":61,"shift":8,"offset":-1,},
    "62": {"x":176,"y":68,"w":11,"h":20,"character":62,"shift":11,"offset":-1,},
    "63": {"x":189,"y":68,"w":6,"h":20,"character":63,"shift":7,"offset":0,},
    "64": {"x":197,"y":68,"w":15,"h":20,"character":64,"shift":16,"offset":0,},
    "65": {"x":214,"y":68,"w":11,"h":20,"character":65,"shift":10,"offset":-1,},
    "66": {"x":227,"y":68,"w":9,"h":20,"character":66,"shift":9,"offset":-1,},
    "67": {"x":238,"y":68,"w":7,"h":20,"character":67,"shift":9,"offset":1,},
    "68": {"x":2,"y":90,"w":9,"h":20,"character":68,"shift":9,"offset":0,},
    "69": {"x":13,"y":90,"w":8,"h":20,"character":69,"shift":9,"offset":0,},
    "70": {"x":23,"y":90,"w":8,"h":20,"character":70,"shift":9,"offset":0,},
    "71": {"x":43,"y":90,"w":9,"h":20,"character":71,"shift":9,"offset":0,},
    "72": {"x":212,"y":90,"w":9,"h":20,"character":72,"shift":9,"offset":-1,},
    "73": {"x":223,"y":90,"w":7,"h":20,"character":73,"shift":8,"offset":0,},
    "74": {"x":232,"y":90,"w":8,"h":20,"character":74,"shift":9,"offset":0,},
    "75": {"x":186,"y":112,"w":9,"h":20,"character":75,"shift":9,"offset":-1,},
    "76": {"x":197,"y":112,"w":8,"h":20,"character":76,"shift":9,"offset":0,},
    "77": {"x":207,"y":112,"w":9,"h":20,"character":77,"shift":9,"offset":0,},
    "78": {"x":218,"y":112,"w":9,"h":20,"character":78,"shift":9,"offset":0,},
    "79": {"x":229,"y":112,"w":7,"h":20,"character":79,"shift":9,"offset":1,},
    "80": {"x":238,"y":112,"w":8,"h":20,"character":80,"shift":9,"offset":0,},
    "81": {"x":2,"y":134,"w":8,"h":20,"character":81,"shift":8,"offset":0,},
    "82": {"x":12,"y":134,"w":10,"h":20,"character":82,"shift":10,"offset":-1,},
    "83": {"x":35,"y":134,"w":8,"h":20,"character":83,"shift":8,"offset":-1,},
    "84": {"x":125,"y":134,"w":8,"h":20,"character":84,"shift":8,"offset":-1,},
    "85": {"x":45,"y":134,"w":10,"h":20,"character":85,"shift":9,"offset":-1,},
    "86": {"x":57,"y":134,"w":8,"h":20,"character":86,"shift":9,"offset":0,},
    "87": {"x":67,"y":134,"w":10,"h":20,"character":87,"shift":10,"offset":-1,},
    "88": {"x":79,"y":134,"w":9,"h":20,"character":88,"shift":9,"offset":-1,},
    "89": {"x":90,"y":134,"w":9,"h":20,"character":89,"shift":9,"offset":-1,},
    "90": {"x":101,"y":134,"w":8,"h":20,"character":90,"shift":8,"offset":0,},
    "91": {"x":111,"y":134,"w":3,"h":20,"character":91,"shift":5,"offset":1,},
    "92": {"x":116,"y":134,"w":7,"h":20,"character":92,"shift":6,"offset":-2,},
    "93": {"x":181,"y":112,"w":3,"h":20,"character":93,"shift":4,"offset":0,},
    "94": {"x":174,"y":112,"w":5,"h":20,"character":94,"shift":6,"offset":0,},
    "95": {"x":163,"y":112,"w":9,"h":20,"character":95,"shift":9,"offset":-1,},
    "96": {"x":158,"y":112,"w":3,"h":20,"character":96,"shift":4,"offset":0,},
    "97": {"x":242,"y":90,"w":7,"h":20,"character":97,"shift":8,"offset":0,},
    "98": {"x":2,"y":112,"w":7,"h":20,"character":98,"shift":7,"offset":-1,},
    "99": {"x":11,"y":112,"w":6,"h":20,"character":99,"shift":7,"offset":0,},
    "100": {"x":19,"y":112,"w":8,"h":20,"character":100,"shift":8,"offset":0,},
    "101": {"x":29,"y":112,"w":6,"h":20,"character":101,"shift":7,"offset":0,},
    "102": {"x":37,"y":112,"w":8,"h":20,"character":102,"shift":8,"offset":-1,},
    "103": {"x":47,"y":112,"w":9,"h":20,"character":103,"shift":9,"offset":-1,},
    "104": {"x":58,"y":112,"w":9,"h":20,"character":104,"shift":10,"offset":0,},
    "105": {"x":69,"y":112,"w":7,"h":20,"character":105,"shift":7,"offset":-1,},
    "106": {"x":78,"y":112,"w":6,"h":20,"character":106,"shift":6,"offset":-1,},
    "107": {"x":86,"y":112,"w":9,"h":20,"character":107,"shift":8,"offset":-1,},
    "108": {"x":97,"y":112,"w":7,"h":20,"character":108,"shift":8,"offset":0,},
    "109": {"x":106,"y":112,"w":10,"h":20,"character":109,"shift":10,"offset":-1,},
    "110": {"x":118,"y":112,"w":10,"h":20,"character":110,"shift":10,"offset":0,},
    "111": {"x":130,"y":112,"w":7,"h":20,"character":111,"shift":8,"offset":0,},
    "112": {"x":139,"y":112,"w":8,"h":20,"character":112,"shift":8,"offset":-1,},
    "113": {"x":149,"y":112,"w":7,"h":20,"character":113,"shift":7,"offset":0,},
    "114": {"x":94,"y":68,"w":8,"h":20,"character":114,"shift":9,"offset":0,},
    "115": {"x":86,"y":68,"w":6,"h":20,"character":115,"shift":7,"offset":0,},
    "116": {"x":76,"y":68,"w":8,"h":20,"character":116,"shift":8,"offset":-1,},
    "117": {"x":152,"y":24,"w":8,"h":20,"character":117,"shift":9,"offset":0,},
    "118": {"x":238,"y":2,"w":8,"h":20,"character":118,"shift":9,"offset":0,},
    "119": {"x":2,"y":24,"w":10,"h":20,"character":119,"shift":10,"offset":-1,},
    "120": {"x":14,"y":24,"w":9,"h":20,"character":120,"shift":9,"offset":0,},
    "121": {"x":25,"y":24,"w":9,"h":20,"character":121,"shift":10,"offset":0,},
    "122": {"x":36,"y":24,"w":7,"h":20,"character":122,"shift":7,"offset":0,},
    "123": {"x":45,"y":24,"w":3,"h":20,"character":123,"shift":5,"offset":1,},
    "124": {"x":50,"y":24,"w":3,"h":20,"character":124,"shift":2,"offset":-1,},
    "125": {"x":55,"y":24,"w":3,"h":20,"character":125,"shift":4,"offset":0,},
    "126": {"x":60,"y":24,"w":6,"h":20,"character":126,"shift":7,"offset":0,},
    "160": {"x":68,"y":24,"w":0,"h":20,"character":160,"shift":6,"offset":0,},
    "161": {"x":70,"y":24,"w":3,"h":20,"character":161,"shift":5,"offset":1,},
    "162": {"x":75,"y":24,"w":6,"h":20,"character":162,"shift":8,"offset":1,},
    "167": {"x":83,"y":24,"w":6,"h":20,"character":167,"shift":7,"offset":0,},
    "168": {"x":91,"y":24,"w":7,"h":20,"character":168,"shift":8,"offset":0,},
    "169": {"x":100,"y":24,"w":10,"h":20,"character":169,"shift":11,"offset":0,},
    "173": {"x":112,"y":24,"w":8,"h":20,"character":173,"shift":8,"offset":0,},
    "174": {"x":122,"y":24,"w":18,"h":20,"character":174,"shift":19,"offset":0,},
    "175": {"x":227,"y":2,"w":9,"h":20,"character":175,"shift":9,"offset":-1,},
    "177": {"x":142,"y":24,"w":8,"h":20,"character":177,"shift":8,"offset":0,},
    "180": {"x":222,"y":2,"w":3,"h":20,"character":180,"shift":4,"offset":0,},
    "181": {"x":193,"y":2,"w":18,"h":20,"character":181,"shift":19,"offset":0,},
    "183": {"x":10,"y":2,"w":3,"h":20,"character":183,"shift":4,"offset":0,},
    "184": {"x":15,"y":2,"w":4,"h":20,"character":184,"shift":4,"offset":-1,},
    "191": {"x":21,"y":2,"w":7,"h":20,"character":191,"shift":8,"offset":0,},
    "192": {"x":30,"y":2,"w":11,"h":20,"character":192,"shift":11,"offset":-1,},
    "193": {"x":43,"y":2,"w":10,"h":20,"character":193,"shift":10,"offset":-1,},
    "194": {"x":55,"y":2,"w":11,"h":20,"character":194,"shift":11,"offset":-1,},
    "195": {"x":68,"y":2,"w":11,"h":20,"character":195,"shift":10,"offset":-1,},
    "196": {"x":81,"y":2,"w":11,"h":20,"character":196,"shift":11,"offset":-1,},
    "197": {"x":94,"y":2,"w":11,"h":20,"character":197,"shift":11,"offset":-1,},
    "198": {"x":107,"y":2,"w":16,"h":20,"character":198,"shift":16,"offset":-1,},
    "199": {"x":125,"y":2,"w":7,"h":20,"character":199,"shift":9,"offset":1,},
    "200": {"x":134,"y":2,"w":8,"h":20,"character":200,"shift":9,"offset":0,},
    "201": {"x":144,"y":2,"w":8,"h":20,"character":201,"shift":9,"offset":0,},
    "202": {"x":154,"y":2,"w":8,"h":20,"character":202,"shift":9,"offset":0,},
    "203": {"x":164,"y":2,"w":9,"h":20,"character":203,"shift":9,"offset":-1,},
    "204": {"x":175,"y":2,"w":7,"h":20,"character":204,"shift":8,"offset":0,},
    "205": {"x":184,"y":2,"w":7,"h":20,"character":205,"shift":8,"offset":0,},
    "206": {"x":213,"y":2,"w":7,"h":20,"character":206,"shift":8,"offset":0,},
    "207": {"x":162,"y":24,"w":7,"h":20,"character":207,"shift":8,"offset":0,},
    "209": {"x":111,"y":46,"w":9,"h":20,"character":209,"shift":9,"offset":0,},
    "210": {"x":171,"y":24,"w":7,"h":20,"character":210,"shift":8,"offset":0,},
    "211": {"x":144,"y":46,"w":7,"h":20,"character":211,"shift":8,"offset":0,},
    "212": {"x":153,"y":46,"w":7,"h":20,"character":212,"shift":8,"offset":0,},
    "214": {"x":162,"y":46,"w":7,"h":20,"character":214,"shift":8,"offset":0,},
    "215": {"x":171,"y":46,"w":7,"h":20,"character":215,"shift":8,"offset":0,},
    "216": {"x":180,"y":46,"w":7,"h":20,"character":216,"shift":9,"offset":1,},
    "217": {"x":189,"y":46,"w":10,"h":20,"character":217,"shift":9,"offset":-1,},
    "218": {"x":201,"y":46,"w":10,"h":20,"character":218,"shift":9,"offset":-1,},
    "219": {"x":213,"y":46,"w":10,"h":20,"character":219,"shift":9,"offset":-1,},
    "220": {"x":225,"y":46,"w":10,"h":20,"character":220,"shift":9,"offset":-1,},
    "221": {"x":237,"y":46,"w":9,"h":20,"character":221,"shift":9,"offset":-1,},
    "223": {"x":2,"y":68,"w":10,"h":20,"character":223,"shift":9,"offset":-1,},
    "224": {"x":14,"y":68,"w":7,"h":20,"character":224,"shift":8,"offset":1,},
    "225": {"x":23,"y":68,"w":7,"h":20,"character":225,"shift":9,"offset":1,},
    "226": {"x":32,"y":68,"w":7,"h":20,"character":226,"shift":8,"offset":1,},
    "227": {"x":41,"y":68,"w":7,"h":20,"character":227,"shift":8,"offset":1,},
    "228": {"x":50,"y":68,"w":7,"h":20,"character":228,"shift":8,"offset":0,},
    "229": {"x":59,"y":68,"w":7,"h":20,"character":229,"shift":9,"offset":1,},
    "230": {"x":131,"y":46,"w":11,"h":20,"character":230,"shift":13,"offset":1,},
    "231": {"x":68,"y":68,"w":6,"h":20,"character":231,"shift":7,"offset":0,},
    "232": {"x":122,"y":46,"w":7,"h":20,"character":232,"shift":7,"offset":-1,},
    "233": {"x":102,"y":46,"w":7,"h":20,"character":233,"shift":7,"offset":-1,},
    "234": {"x":180,"y":24,"w":7,"h":20,"character":234,"shift":7,"offset":-1,},
    "235": {"x":189,"y":24,"w":8,"h":20,"character":235,"shift":7,"offset":-1,},
    "236": {"x":199,"y":24,"w":7,"h":20,"character":236,"shift":8,"offset":0,},
    "237": {"x":208,"y":24,"w":7,"h":20,"character":237,"shift":8,"offset":0,},
    "238": {"x":217,"y":24,"w":7,"h":20,"character":238,"shift":8,"offset":0,},
    "239": {"x":226,"y":24,"w":8,"h":20,"character":239,"shift":8,"offset":-1,},
    "241": {"x":236,"y":24,"w":9,"h":20,"character":241,"shift":10,"offset":0,},
    "242": {"x":2,"y":46,"w":7,"h":20,"character":242,"shift":8,"offset":0,},
    "243": {"x":11,"y":46,"w":7,"h":20,"character":243,"shift":8,"offset":0,},
    "244": {"x":20,"y":46,"w":7,"h":20,"character":244,"shift":8,"offset":0,},
    "246": {"x":29,"y":46,"w":8,"h":20,"character":246,"shift":7,"offset":-1,},
    "247": {"x":39,"y":46,"w":9,"h":20,"character":247,"shift":8,"offset":-1,},
    "248": {"x":50,"y":46,"w":8,"h":20,"character":248,"shift":8,"offset":-1,},
    "249": {"x":60,"y":46,"w":9,"h":20,"character":249,"shift":9,"offset":0,},
    "250": {"x":71,"y":46,"w":8,"h":20,"character":250,"shift":8,"offset":0,},
    "251": {"x":81,"y":46,"w":8,"h":20,"character":251,"shift":8,"offset":0,},
    "252": {"x":91,"y":46,"w":9,"h":20,"character":252,"shift":8,"offset":-1,},
    "253": {"x":24,"y":134,"w":9,"h":20,"character":253,"shift":9,"offset":0,},
    "255": {"x":135,"y":134,"w":9,"h":20,"character":255,"shift":9,"offset":0,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":255,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Schriftarten",
    "path": "folders/Schriftarten.yy",
  },
  "resourceVersion": "1.0",
  "name": "fCreditsOnScreen",
  "tags": [],
  "resourceType": "GMFont",
}
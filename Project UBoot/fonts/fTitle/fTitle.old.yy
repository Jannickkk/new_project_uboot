{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "ELEGANT TYPEWRITER",
  "styleName": "Regular",
  "size": 80.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 1,
  "glyphs": {
    "32": {"x":2,"y":2,"w":34,"h":124,"character":32,"shift":34,"offset":0,},
    "33": {"x":790,"y":254,"w":17,"h":124,"character":33,"shift":29,"offset":5,},
    "34": {"x":761,"y":254,"w":27,"h":124,"character":34,"shift":34,"offset":-1,},
    "35": {"x":711,"y":254,"w":48,"h":124,"character":35,"shift":55,"offset":-1,},
    "36": {"x":660,"y":254,"w":49,"h":124,"character":36,"shift":60,"offset":6,},
    "37": {"x":608,"y":254,"w":50,"h":124,"character":37,"shift":59,"offset":1,},
    "38": {"x":551,"y":254,"w":55,"h":124,"character":38,"shift":62,"offset":0,},
    "39": {"x":538,"y":254,"w":11,"h":124,"character":39,"shift":18,"offset":0,},
    "40": {"x":518,"y":254,"w":18,"h":124,"character":40,"shift":25,"offset":1,},
    "41": {"x":498,"y":254,"w":18,"h":124,"character":41,"shift":24,"offset":-1,},
    "42": {"x":809,"y":254,"w":48,"h":124,"character":42,"shift":55,"offset":0,},
    "43": {"x":452,"y":254,"w":44,"h":124,"character":43,"shift":51,"offset":0,},
    "44": {"x":394,"y":254,"w":17,"h":124,"character":44,"shift":24,"offset":0,},
    "45": {"x":347,"y":254,"w":45,"h":124,"character":45,"shift":52,"offset":0,},
    "46": {"x":327,"y":254,"w":18,"h":124,"character":46,"shift":25,"offset":0,},
    "47": {"x":281,"y":254,"w":44,"h":124,"character":47,"shift":42,"offset":-1,},
    "48": {"x":242,"y":254,"w":37,"h":124,"character":48,"shift":44,"offset":0,},
    "49": {"x":222,"y":254,"w":18,"h":124,"character":49,"shift":25,"offset":1,},
    "50": {"x":182,"y":254,"w":38,"h":124,"character":50,"shift":46,"offset":1,},
    "51": {"x":143,"y":254,"w":37,"h":124,"character":51,"shift":45,"offset":1,},
    "52": {"x":99,"y":254,"w":42,"h":124,"character":52,"shift":48,"offset":-1,},
    "53": {"x":413,"y":254,"w":37,"h":124,"character":53,"shift":45,"offset":0,},
    "54": {"x":859,"y":254,"w":38,"h":124,"character":54,"shift":44,"offset":-1,},
    "55": {"x":899,"y":254,"w":41,"h":124,"character":55,"shift":47,"offset":-1,},
    "56": {"x":942,"y":254,"w":42,"h":124,"character":56,"shift":50,"offset":0,},
    "57": {"x":2,"y":506,"w":38,"h":124,"character":57,"shift":44,"offset":0,},
    "58": {"x":981,"y":380,"w":16,"h":124,"character":58,"shift":23,"offset":0,},
    "59": {"x":964,"y":380,"w":15,"h":124,"character":59,"shift":22,"offset":0,},
    "60": {"x":899,"y":380,"w":63,"h":124,"character":60,"shift":70,"offset":0,},
    "61": {"x":851,"y":380,"w":46,"h":124,"character":61,"shift":52,"offset":-1,},
    "62": {"x":787,"y":380,"w":62,"h":124,"character":62,"shift":70,"offset":0,},
    "63": {"x":746,"y":380,"w":39,"h":124,"character":63,"shift":51,"offset":5,},
    "64": {"x":657,"y":380,"w":87,"h":124,"character":64,"shift":94,"offset":0,},
    "65": {"x":598,"y":380,"w":57,"h":124,"character":65,"shift":64,"offset":-1,},
    "66": {"x":548,"y":380,"w":48,"h":124,"character":66,"shift":54,"offset":-1,},
    "67": {"x":505,"y":380,"w":41,"h":124,"character":67,"shift":49,"offset":0,},
    "68": {"x":454,"y":380,"w":49,"h":124,"character":68,"shift":55,"offset":0,},
    "69": {"x":403,"y":380,"w":49,"h":124,"character":69,"shift":54,"offset":-2,},
    "70": {"x":354,"y":380,"w":47,"h":124,"character":70,"shift":54,"offset":0,},
    "71": {"x":304,"y":380,"w":48,"h":124,"character":71,"shift":55,"offset":0,},
    "72": {"x":253,"y":380,"w":49,"h":124,"character":72,"shift":56,"offset":0,},
    "73": {"x":211,"y":380,"w":40,"h":124,"character":73,"shift":47,"offset":0,},
    "74": {"x":160,"y":380,"w":49,"h":124,"character":74,"shift":56,"offset":0,},
    "75": {"x":106,"y":380,"w":52,"h":124,"character":75,"shift":60,"offset":-1,},
    "76": {"x":56,"y":380,"w":48,"h":124,"character":76,"shift":56,"offset":0,},
    "77": {"x":2,"y":380,"w":52,"h":124,"character":77,"shift":60,"offset":0,},
    "78": {"x":48,"y":254,"w":49,"h":124,"character":78,"shift":55,"offset":-1,},
    "79": {"x":2,"y":254,"w":44,"h":124,"character":79,"shift":52,"offset":0,},
    "80": {"x":968,"y":128,"w":47,"h":124,"character":80,"shift":55,"offset":0,},
    "81": {"x":942,"y":2,"w":46,"h":124,"character":81,"shift":50,"offset":1,},
    "82": {"x":865,"y":2,"w":55,"h":124,"character":82,"shift":61,"offset":-1,},
    "83": {"x":820,"y":2,"w":43,"h":124,"character":83,"shift":50,"offset":-1,},
    "84": {"x":775,"y":2,"w":43,"h":124,"character":84,"shift":49,"offset":-1,},
    "85": {"x":722,"y":2,"w":51,"h":124,"character":85,"shift":57,"offset":-2,},
    "86": {"x":672,"y":2,"w":48,"h":124,"character":86,"shift":55,"offset":0,},
    "87": {"x":617,"y":2,"w":53,"h":124,"character":87,"shift":59,"offset":-1,},
    "88": {"x":566,"y":2,"w":49,"h":124,"character":88,"shift":56,"offset":0,},
    "89": {"x":516,"y":2,"w":48,"h":124,"character":89,"shift":55,"offset":-1,},
    "90": {"x":470,"y":2,"w":44,"h":124,"character":90,"shift":50,"offset":-1,},
    "91": {"x":922,"y":2,"w":18,"h":124,"character":91,"shift":25,"offset":1,},
    "92": {"x":424,"y":2,"w":44,"h":124,"character":92,"shift":42,"offset":-10,},
    "93": {"x":356,"y":2,"w":18,"h":124,"character":93,"shift":24,"offset":-1,},
    "94": {"x":325,"y":2,"w":29,"h":124,"character":94,"shift":38,"offset":1,},
    "95": {"x":269,"y":2,"w":54,"h":124,"character":95,"shift":55,"offset":-5,},
    "96": {"x":250,"y":2,"w":17,"h":124,"character":96,"shift":25,"offset":0,},
    "97": {"x":205,"y":2,"w":43,"h":124,"character":97,"shift":50,"offset":0,},
    "98": {"x":159,"y":2,"w":44,"h":124,"character":98,"shift":47,"offset":-4,},
    "99": {"x":121,"y":2,"w":36,"h":124,"character":99,"shift":44,"offset":0,},
    "100": {"x":77,"y":2,"w":42,"h":124,"character":100,"shift":50,"offset":1,},
    "101": {"x":38,"y":2,"w":37,"h":124,"character":101,"shift":44,"offset":-1,},
    "102": {"x":376,"y":2,"w":46,"h":124,"character":102,"shift":54,"offset":0,},
    "103": {"x":2,"y":128,"w":47,"h":124,"character":103,"shift":54,"offset":-1,},
    "104": {"x":435,"y":128,"w":48,"h":124,"character":104,"shift":55,"offset":0,},
    "105": {"x":51,"y":128,"w":39,"h":124,"character":105,"shift":44,"offset":-3,},
    "106": {"x":896,"y":128,"w":32,"h":124,"character":106,"shift":39,"offset":0,},
    "107": {"x":849,"y":128,"w":45,"h":124,"character":107,"shift":51,"offset":-1,},
    "108": {"x":807,"y":128,"w":40,"h":124,"character":108,"shift":47,"offset":0,},
    "109": {"x":755,"y":128,"w":50,"h":124,"character":109,"shift":58,"offset":0,},
    "110": {"x":704,"y":128,"w":49,"h":124,"character":110,"shift":56,"offset":0,},
    "111": {"x":663,"y":128,"w":39,"h":124,"character":111,"shift":46,"offset":0,},
    "112": {"x":619,"y":128,"w":42,"h":124,"character":112,"shift":49,"offset":-1,},
    "113": {"x":574,"y":128,"w":43,"h":124,"character":113,"shift":44,"offset":-1,},
    "114": {"x":527,"y":128,"w":45,"h":124,"character":114,"shift":53,"offset":0,},
    "115": {"x":930,"y":128,"w":36,"h":124,"character":115,"shift":43,"offset":-1,},
    "116": {"x":485,"y":128,"w":40,"h":124,"character":116,"shift":44,"offset":-2,},
    "117": {"x":388,"y":128,"w":45,"h":124,"character":117,"shift":50,"offset":-1,},
    "118": {"x":338,"y":128,"w":48,"h":124,"character":118,"shift":55,"offset":0,},
    "119": {"x":282,"y":128,"w":54,"h":124,"character":119,"shift":60,"offset":-1,},
    "120": {"x":232,"y":128,"w":48,"h":124,"character":120,"shift":55,"offset":0,},
    "121": {"x":181,"y":128,"w":49,"h":124,"character":121,"shift":55,"offset":-1,},
    "122": {"x":141,"y":128,"w":38,"h":124,"character":122,"shift":44,"offset":0,},
    "123": {"x":121,"y":128,"w":18,"h":124,"character":123,"shift":25,"offset":1,},
    "124": {"x":112,"y":128,"w":7,"h":124,"character":124,"shift":16,"offset":2,},
    "125": {"x":92,"y":128,"w":18,"h":124,"character":125,"shift":24,"offset":-1,},
    "126": {"x":42,"y":506,"w":37,"h":124,"character":126,"shift":45,"offset":1,},
    "9647": {"x":81,"y":506,"w":62,"h":124,"character":9647,"shift":104,"offset":21,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Schriftarten",
    "path": "folders/Schriftarten.yy",
  },
  "resourceVersion": "1.0",
  "name": "fTitle",
  "tags": [],
  "resourceType": "GMFont",
}
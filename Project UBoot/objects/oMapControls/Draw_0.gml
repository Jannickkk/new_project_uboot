// Zeige Name des jeweiligen Raumes an
if global.showUG2 = false
{
	if (mouse_x > 689 and mouse_x < 738 and mouse_y > 539 and mouse_y < 609) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Schleuse"));
	}
	
	if (mouse_x > 689 and mouse_x < 976 and mouse_y > 365 and mouse_y < 464) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Offiziersmesse"));
	}
	
	if (mouse_x > 741 and mouse_x < 976 and mouse_y > 510 and mouse_y < 609) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Brücke"));
	}
	
	if (mouse_x > 979 and mouse_x < 1267 and mouse_y > 365 and mouse_y < 464) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Kommandantenkabine"));
	}
	
	if (mouse_x > 1028 and mouse_x < 1267 and mouse_y > 516 and mouse_y < 609) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Kombüse"));
	}
	
	if (mouse_x > 1472 and mouse_x < 1623 and mouse_y > 442 and mouse_y < 540) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Bugtorpedoraum"));
	}
	
	if (mouse_x > 382 and mouse_x < 533 and mouse_y > 442 and mouse_y < 540) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Hecktorpedoraum"));
	}
}

if global.showUG2 = true
{
	if (mouse_x > 1243 and mouse_x < 1360 and mouse_y > 483 and mouse_y < 612) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Mannschaftsquartiere"));
	}
	
	if (mouse_x > 1300 and mouse_x < 1360 and mouse_y > 388 and mouse_y < 479) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Mannschaftsquartiere"));
	}
	
	if (mouse_x > 1362 and mouse_x < 1421 and mouse_y > 483 and mouse_y < 612) 
	{
		if global.foundAmericanFlag = false
		{
			draw_set_font(fMapLocation);
			draw_set_color(c_white);
			draw_set_halign(fa_left);
			draw_text(60, 32, string("???"));
		}
		
		if global.foundAmericanFlag = true
		{
			draw_set_font(fMapLocation);
			draw_set_color(c_white);
			draw_set_halign(fa_left);
			draw_text(60, 32, string("Amerikanisches Lager"));
		}
	}
	
	if (mouse_x > 1425 and mouse_x < 1479 and mouse_y > 483 and mouse_y < 612) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Tauchkapselraum"));
	}
	
	if (mouse_x > 1016 and mouse_x < 1168 and mouse_y > 360 and mouse_y < 434) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Maschinenraum A"));
	}
	
	if (mouse_x > 1016 and mouse_x < 1239 and mouse_y > 437 and mouse_y < 569) 
	{
		draw_set_font(fMapLocation);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(60, 32, string("Maschinenraum B"));
	}
}
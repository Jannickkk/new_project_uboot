if global.lostPartner = false
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Andrej: Die Eingangstür des U-Bootes.");
	AddDialogue(mydialogue, "Andrej: Zwei Stunden nach Beginn der Mission werden wir hier wieder abgeholt.");
	PlayDialogue(mydialogue);
}

if global.lostPartner = true
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Andrej: Die Eingangstür des U-Bootes.");
	AddDialogue(mydialogue, "Andrej: Zwei Stunden nach Beginn der Mission werden wir hier wieder abgeholt.");
	AddDialogue(mydialogue, "Andrej: Ich muss Dimitri unbedingt finden...!");
	PlayDialogue(mydialogue);
}
draw_set_font(fGame);
draw_set_color(c_white);

if (position_meeting(mouse_x, mouse_y, id))
{
  draw_sprite(sSelected, image_index, x, y);
}
else
{
  draw_self();
}
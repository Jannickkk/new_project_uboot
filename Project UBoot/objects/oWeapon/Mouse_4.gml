mydialogue = CreateDialogue();
AddDialogue(mydialogue, "Andrej: Auf der Notiz in der Kombüse stand, dass hier eine Waffe sein soll.");
AddDialogue(mydialogue, "Andrej: Hinter dem Schrank ist ein kleiner Spalt. Mal sehen...");
AddDialogue(mydialogue, "Andrej: Was, eine Pistole?! Wieso bewahrt man so etwas hier auf?");
AddDialogue(mydialogue, "Andrej: Vielleicht sollte ich sie mitnehmen. Könnte sich als nützlich erweisen.");
PlayDialogue(mydialogue);

global.weaponFound = true
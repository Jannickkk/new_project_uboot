/// Zeige den Künstler
draw_set_font(fMapExplanation);
draw_set_color(c_aqua);
draw_set_halign(fa_left);
draw_text(10, 1050, string("Artwork by Gregory Shevtsov"));
draw_set_color(c_white);
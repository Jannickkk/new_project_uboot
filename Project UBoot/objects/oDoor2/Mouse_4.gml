if global.door2Locked = true and global.codewordFound = false
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Die Tür ist verschlossen.");
	AddDialogue(mydialogue, "Andrej: Sieht aus, als bräuchte man hier einen Zahlencode.");
	PlayDialogue(mydialogue);
}

if global.codewordFound = true
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Andrej: Ich hoffe, dieser Zahlencode ist der Richtige...");
	AddDialogue(mydialogue, "Die Tür wurde aufgeschlossen.");
	AddDialogue(mydialogue, "Andrej: Geht doch.");
	PlayDialogue(mydialogue);
	global.door2Unlocked = true;
}
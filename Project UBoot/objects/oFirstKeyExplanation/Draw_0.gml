// Zeige Namen und Nutzen des Gegenstandes an
if (position_meeting(mouse_x, mouse_y, id))
{
	draw_set_font(fMapExplanation);
	draw_set_color(c_white);
	draw_set_halign(fa_left);
	draw_text(60, 42, string("Schlüssel zum U-Boot: Ein vom Hauptquartier bereitgestellter Schlüssel, der die Schleuse des U-Boots öffnen soll."));
}
if global.foundFoodClue = false
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Andrej: Lauter Kisten mit Nahrungsvorräten.");
	PlayDialogue(mydialogue);
}

if global.foundFoodClue = true
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Andrej: Lauter Kisten mit Nahrungsvorräten.");
	AddDialogue(mydialogue, "Andrej: Wieso ist hier so viel davon, wenn der Rest der Besatzung hungern");
	PlayDialogue(mydialogue);
}
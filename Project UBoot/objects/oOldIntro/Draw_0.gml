//Schriftart
draw_set_font(fGame);
draw_set_halign(fa_left);
draw_set_color(c_white);
 
draw_text(175, 100, "Hintergrundgeschichte:");
draw_text(175, 200, "Nachdem ein U-Boot der Sowjetunion, das vor 25 Jahren");
draw_text(175, 250, "verschollen ist, in der Nordsee wieder auftaucht, werden");
draw_text(175, 300, "Leutnant Andrej Iwanow und Unteroffizier Dimitri Sergejew");
draw_text(175, 350, "dorthin entsandt, um die dort befindlichen Dokumente über");
draw_text(175, 400, "die damalige Militäroperation zu bergen.");

/*Nachdem ein U-Boot der Sowjetunion, das vor 25 Jahren verschollen ist, in der Nordsee
wieder auftaucht, werden Leutnant Andrej Iwanow und Unteroffizier
Dimitri Sergejew dorthin entsandt, um die dort befindlichen Dokumente über die damalige
Militäroperation zu bergen.
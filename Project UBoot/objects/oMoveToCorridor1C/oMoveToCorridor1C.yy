{
  "spriteId": {
    "name": "sMoveUpSmall",
    "path": "sprites/sMoveUpSmall/sMoveUpSmall.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": null,
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":8,"collisionObjectId":null,"parent":{"name":"oMoveToCorridor1C","path":"objects/oMoveToCorridor1C/oMoveToCorridor1C.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":4,"eventType":6,"collisionObjectId":null,"parent":{"name":"oMoveToCorridor1C","path":"objects/oMoveToCorridor1C/oMoveToCorridor1C.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"oMoveToCorridor1C","path":"objects/oMoveToCorridor1C/oMoveToCorridor1C.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"oMoveToCorridor1C","path":"objects/oMoveToCorridor1C/oMoveToCorridor1C.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Corridor1A",
    "path": "folders/Objekte/Game/Room objects/Corridor1A.yy",
  },
  "resourceVersion": "1.0",
  "name": "oMoveToCorridor1C",
  "tags": [],
  "resourceType": "GMObject",
}
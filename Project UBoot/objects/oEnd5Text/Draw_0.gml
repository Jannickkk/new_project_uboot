draw_set_font(fGame);
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_text(250, 300, "Andrej floh mit Tauchkapsel ohne Dimitri zu retten von dem U-Boot, das kurz nach");
draw_text(250, 350, "seiner Flucht explodierte. Das U-Boot wurde von einem weiteren Suchtrupp zerstört");
draw_text(250, 400, "vorgefunden und weder Dimitri noch Andrej wurden jemals wieder gesehen.");
draw_text(250, 450, "Es wird vermutet, dass die geheime Waffe beim Bergen versehentlich ausgelöst worden");
draw_text(250, 500, "ist und die Explosion des U-Boots verursachte.");
draw_text(250, 550, "Die wahren Gründe werden wohl für immer auf dem Meeresgrund bleiben...");
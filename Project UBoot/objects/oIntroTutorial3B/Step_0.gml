/// Zeige die letzte Textzeile
if keyboard_check_pressed(vk_space) and global.playedIntroSoundEffect = false
{
	instance_create_layer(x, y, "Instances", oIntroTutorialLastText);
	audio_play_sound(So_creepySound, 1, 0);
	global.playedIntroSoundEffect = true
}
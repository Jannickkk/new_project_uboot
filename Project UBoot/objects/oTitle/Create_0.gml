// set Global variables:

// Räume initialisieren:
global.roomNumber = 0;

// Intro
global.readBriefing = false;
global.metDimitriIntro = false;
global.playedIntroSoundEffect = false;

// Erste Tür ist verschlossen
global.doorInteracted = false;
global.doorInteractedAndOpened = false;

// Zweite Tür ist verschlossen
global.door2Locked = true;
global.door2Unlocked = false;

// Überlebender wird gefunden, Dimitri bleibt zurück
global.waitingDimitri = false;

// Zerstörte Brücke wird entdeckt
global.foundDestroyedBridge = false;

// Andrej kehrt zurück in die Offiziersmesse, doch Dimitri und der Überlebende sind verschwunden
global.lostPartner = false;

// Verschlossene Tür wird gefunden
global.foundBlood = false;

// Verschlossene Tür des Saboteuers wird gefunden, der Schlüssel gesucht und die Tür aufgeschlossen
global.foundKey3 = false;
global.door3Unlocked = false;
global.foundDoor3 = false;
global.foundAmericanFlag = false;
global.foundEmergencyExit = false;
global.foundFoodClue = false

// Finde den Code im Maschinenraum A
global.codewordFound = false;

// Ende
global.metSurvivorEnd = false;
global.showNextText = false;
global.showNextText2 = false


// Inventar:
// 1. Der erste Schlüssel (Schleuse) befindet sich von Anfang an im Besitz des Spielers

// 2. Der zweite Schlüssel (Brücke) wird vom Überlebenden überreicht
global.gotKey2 = false;

// 3. Der dritte Schlüssel (Saboteur) wird optional in den Mannschaftsquartieren gefunden
global.gotKey3 = false;

// 4. Der Ausweis des Saboteurs kann in seinem Lager gefunden werden, um die wahre Identität des Überlebenden aufzudecken
global.foundPassport = false;

// 5. Der Code wird dem Inventar nach Finden der Notiz im Maschinenraum A hinzugefügt

// 6. Die Waffe kann in der Offiziersmesse gefunden werden, wenn zuvor Hinweise auf ihren Verbleib gefunden werden
global.weaponClueFound = false;
global.weaponFound = false;

// 7. Dimitris Verlobungsring kann gefunden werden, um ein alternatives Ende freizuschalten
global.weddingRingFound = false;


// Musik:
audio_stop_all();
audio_play_sound(So_titlescreen, 1, 99);
global.playedSound = false;
global.playedSound2 = false;
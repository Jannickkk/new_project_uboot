mydialogue = CreateDialogue();
AddDialogue(mydialogue, "Andrej: Jetzt verstehe ich gar nichts mehr... eine amerikanische Flagge?");
AddDialogue(mydialogue, "Andrej: Wieso hängt hier die Flagge unseres Feindes?");
AddDialogue(mydialogue, "Andrej: Sieht aus, als hätte sich hier ein Eindringling an Bord geschlichen...");
PlayDialogue(mydialogue);

if global.foundAmericanFlag = false
{
	global.foundAmericanFlag = true
}
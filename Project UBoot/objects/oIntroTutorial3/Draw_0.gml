draw_set_font(fGame);
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_text(350, 300, "Am nächsten Tag werden Leutnant Andrej Iwanow und Unteroffizier");
draw_text(350, 350, "Dimitri Sergejew zu dem U-Boot entsandt, um Hinweise über die dort");
draw_text(350, 400, "befindliche Militärwaffe und eventuelle Überlebende zu bergen.");
draw_text(350, 450, "Ihre Mannschaft setzt sie mit dem Versprechen ab, sie zwei Stunden");
draw_text(350, 500, "nach Beginn des Einsatzes wieder abzuholen.");
mydialogue = CreateDialogue();
AddDialogue(mydialogue, "Andrej: An der Tür ist frisches Blut.");
AddDialogue(mydialogue, "Andrej: Ob Dimitri hier wohl vorbeigekommen ist?");
AddDialogue(mydialogue, "Andrej: Die Tür ist verriegelt. Ich brauche einen Code, um sie zu öffnen.");
PlayDialogue(mydialogue);

global.foundBlood = true

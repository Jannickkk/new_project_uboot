// Zeige Tutorial
draw_set_font(fTutorial);
draw_set_color(c_aqua);
draw_set_halign(fa_left);
draw_text(60, 32, string("Klicke mit der Maus auf gekennzeichnete Objekte, um mit ihnen zu interagieren."));
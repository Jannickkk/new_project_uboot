if global.foundKey3 = true
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Die Tür wurde aufgeschlossen.");
	PlayDialogue(mydialogue);
	
	global.door3Unlocked = true
}

else
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Der Schrank ist verschlossen.");
	AddDialogue(mydialogue, "Andrej: Vielleicht gibt es in der Nähe einen Schlüssel?");
	AddDialogue(mydialogue, "Andrej: Ich sollte mich hier im Raum mal umsehen.");
	PlayDialogue(mydialogue);
}
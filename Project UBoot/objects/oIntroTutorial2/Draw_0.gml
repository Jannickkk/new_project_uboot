// Zeige Tutorial
if global.metDimitriIntro = false
{
	draw_set_font(fTutorial);
	draw_set_color(c_aqua);
	draw_set_halign(fa_left);
	draw_text(60, 32, string("Klicke auf einen Charakter, um mit ihm zu sprechen. Setze das Spiel im Dialog durch Drücken der Leertaste fort."));
}
else
{
}
// Schließe Map
audio_play_sound(So_MenuClose, 1, 0);

if global.roomNumber = 1
{
	room_goto(rRoom1A);
}

if global.roomNumber = 2
{
	room_goto(rRoom1B);
}

if global.roomNumber = 3
{
	room_goto(rRoom2A);
}

if global.roomNumber = 4
{
	room_goto(rRoom2B);
}

if global.roomNumber = 5
{
	room_goto(rRoom2C);
}

if global.roomNumber = 6
{
	room_goto(rRoom2E);
}

if global.roomNumber = 7
{
	room_goto(rRoom2G);
}

if global.roomNumber = 8
{
	room_goto(rRoom2H);
}

if global.roomNumber = 9
{
	room_goto(rRoom3A);
}

if global.roomNumber = 10
{
	room_goto(rRoom4A);
}

if global.roomNumber = 11
{
	room_goto(rRoom5A);
}

if global.roomNumber = 12
{
	room_goto(rRoom5B);
}

if global.roomNumber = 13
{
	room_goto(rRoom6A);
}

if global.roomNumber = 14
{
	room_goto(rRoom6B);
}

if global.roomNumber = 15
{
	room_goto(rRoom6C);
}

if global.roomNumber = 16
{
	room_goto(rRoom6D);
}

if global.roomNumber = 17
{
	room_goto(rRoom6E);
}

if global.roomNumber = 18
{
	room_goto(rRoom7A);
}

if global.roomNumber = 19
{
	room_goto(rRoom7B);
}

if global.roomNumber = 20
{
	room_goto(rRoom7D);
}

if global.roomNumber = 21
{
	room_goto(rRoom7E);
}

if global.roomNumber = 22
{
	room_goto(rRoom7F);
}

if global.roomNumber = 23
{
	room_goto(rRoom7G);
}

if global.roomNumber = 24
{
	room_goto(rRoom7H);
}

if global.roomNumber = 25
{
	room_goto(rRoom7I);
}

if global.roomNumber = 26
{
	room_goto(rRoom8A);
}

if global.roomNumber = 27
{
	room_goto(rRoom8B);
}

if global.roomNumber = 28
{
	room_goto(rRoom9A);
}

if global.roomNumber = 29
{
	room_goto(rRoom10A);
}

if global.roomNumber = 30
{
	room_goto(rRoom10B);
}

if global.roomNumber = 31
{
	room_goto(rRoom11A);
}

if global.roomNumber = 32
{
	room_goto(rRoom11B);
}

if global.roomNumber = 33
{
	room_goto(rRoom11C);
}

if global.roomNumber = 34
{
	room_goto(rRoom11D);
}

if global.roomNumber = 35
{
	room_goto(rRoom11E);
}

if global.roomNumber = 36
{
	room_goto(rRoom11F);
}

if global.roomNumber = 37
{
	room_goto(rCorridor1A);
}

if global.roomNumber = 38
{
	room_goto(rCorridor1C);
}

if global.roomNumber = 39
{
	room_goto(rCorridor2A);
}

if global.roomNumber = 40
{
	room_goto(rCorridor2B);
}

if global.roomNumber = 41
{
	room_goto(rCorridor2C);
}

if global.roomNumber = 42
{
	room_goto(rCorridor3A);
}

if global.roomNumber = 43
{
	room_goto(rCorridor3B);
}

if global.roomNumber = 44
{
	room_goto(rCorridor3C);
}

if global.roomNumber = 45
{
	room_goto(rDoor2);
}

if global.roomNumber = 46
{
	room_goto(rDoor3);
}

if global.roomNumber = 47
{
	room_goto(rDoor2B);
}
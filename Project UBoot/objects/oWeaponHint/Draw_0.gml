/// Zeige ein Fragezeichen, um dem Spieler den Weg zu zeigen
if global.weaponClueFound = true and global.weaponFound = false
{
	if global.showUG2 = false
	{
		draw_set_font(fMapExplanation);
		draw_set_color(c_red);
		draw_text(716, 429, "?");
		draw_set_color(c_white);
	}
}
else
{
}
/// Zeige einen Text, um dem Spieler das momentane Ziel zu verdeutlichen

draw_set_font(fMapExplanation);
draw_set_color(c_white);
	
// Schließe die Schleusentür auf
if global.doorInteractedAndOpened = false
{
	draw_text(60, 150, "Ziel: Schließe die Schleusentür auf");
}

// Durchsuche die Räume in der Nähe des Eingangs
if global.doorInteractedAndOpened = true and global.waitingDimitri = false
{
	draw_text(60, 150, "Ziel: Durchsuche die Räume in der Nähe des Eingangs");
}

// Untersuche die Brücke
if global.waitingDimitri = true and global.foundDestroyedBridge = false
{
	draw_text(60, 150, "Ziel: Untersuche die Brücke");
}

// Kehre zu Dimitri zurück
if global.foundDestroyedBridge = true and global.lostPartner = false
{
	draw_text(60, 150, "Ziel: Kehre zu Dimitri zurück");
}

// Folge den Verschwundenen
if global.lostPartner = true and global.foundBlood = false
{
	draw_text(60, 150, "Ziel: Folge den Verschwundenen");
}

// Suche nach dem Code für die verschlossene Tür
if global.foundBlood = true and global.codewordFound = false
{
	draw_text(60, 150, "Ziel: Suche nach dem Code für die verschlossene Tür");
}

// Kehre zu der verschlossenen Tür zurück
if global.codewordFound = true and global.door2Unlocked = false
{
	draw_text(60, 150, "Ziel: Kehre zu der verschlossenen Tür zurück");
}

// Folge weiter den Verschwundenen
if global.door2Unlocked = true
{
	draw_text(60, 150, "Ziel: Folge weiter den Verschwundenen");
}


// Zeige Namen und Nutzen des Gegenstandes an
if (position_meeting(mouse_x, mouse_y, id))
{
	draw_set_font(fMapExplanation);
	draw_set_color(c_white);
	draw_set_halign(fa_left);
	draw_text(60, 27, string("Ausweis eines Amerikaners: Dieser Ausweis befand sich in einem verriegelten Raum. Er lässt vermuten, dass sich ein Amerikaner"));
	draw_text(60, 57, string("unter der Besatzung des U-Bootes befand."));
}
mydialogue = CreateDialogue();
AddDialogue(mydialogue, "Dimitri: Verdammt, siehst du das? Wie konnte jemand so lange hier überleben?");
AddDialogue(mydialogue, "Überlebender: HILFE...! Hilfe...");
AddDialogue(mydialogue, "Überlebender: ...Die Brücke... alles ist auf der Brücke...");
AddDialogue(mydialogue, "Überlebender: Der Schlüssel... nimm diesen Schlüssel... Die Brücke, bitte.");
AddDialogue(mydialogue, "Andrej: Okay. Dimitri, bleib bei dem offensichtlich traumatisierten Überlebenden.");
AddDialogue(mydialogue, "Andrej: Ich gehe zur Brücke und sehe nach, was es damit auf sich hat.");
PlayDialogue(mydialogue);

    
instance_create_layer(1250, 730, "Instances", oDimitri);
global.waitingDimitri = true
global.gotKey2 = true
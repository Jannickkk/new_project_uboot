if global.waitingDimitri = false
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Andrej: Ein Herz und Anya... da war wohl jemand verliebt.");
	AddDialogue(mydialogue, "Dimitri: Genau wie wir, Andrejewitsch.");
	AddDialogue(mydialogue, "Andrej: Süß, aber wir sollten uns auf die Mission konzentrieren.");
	PlayDialogue(mydialogue);
}

if global.waitingDimitri = true
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Andrej: Ein Herz und Anya... da war wohl jemand verliebt.");
	PlayDialogue(mydialogue);
}
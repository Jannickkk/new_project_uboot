mydialogue = CreateDialogue();
AddDialogue(mydialogue, "Andrej: Nanu? Im Aktenschrank liegt ein amerikanischer Ausweis...");
AddDialogue(mydialogue, "Andrej: In der Besatzung sollten keine Amerikaner gewesen sein. Was hat das zu bedeuten?");
AddDialogue(mydialogue, "Andrej: Moment - das Bild auf dem Ausweis sieht genau so aus, wie dieser Überlebende vorhin!");
AddDialogue(mydialogue, "Andrej: Ist dieser Typ in Wirklichkeit etwa...?");
PlayDialogue(mydialogue);

global.foundPassport = true


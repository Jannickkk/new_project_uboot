/// Zerstöre diese Sprechbalse, sobald mit dem Überlebenden interagiert worden ist
if global.waitingDimitri = true
{
	instance_destroy(oSurvivorSpeechBubble);
}
mydialogue = CreateDialogue();
AddDialogue(mydialogue, "Andrej: Die Geräte sind allesamt defekt...");
AddDialogue(mydialogue, "Andrej: Empfang gibt es hier natürlich auch nicht.");
AddDialogue(mydialogue, "Andrej: Ich sollte zurück zu Dimitri und dem Überlebenden gehen.");
AddDialogue(mydialogue, "Andrej: Vielleicht weiß der Überlebende, was hier passiert ist.");
PlayDialogue(mydialogue);

global.foundDestroyedBridge = true;
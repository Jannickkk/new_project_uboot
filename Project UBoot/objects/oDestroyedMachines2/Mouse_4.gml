if global.lostPartner = false
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Andrej: Was ist hier nur passiert?");
	AddDialogue(mydialogue, "Andrej: Vielleicht finde ich Hinweise darüber, was hier vorgefallen ist.");
	AddDialogue(mydialogue, "Andrej: Ich sollte zu Dimitri zurückkehren, sobald ich was gefunden habe.");
	PlayDialogue(mydialogue);
}

else
{
	mydialogue = CreateDialogue();
	AddDialogue(mydialogue, "Die Maschinen sind allesamt zerstört worden.");
	PlayDialogue(mydialogue);
}
if global.showNextText = false
{
	draw_set_font(fGame);
	draw_set_color(c_white);
	draw_set_halign(fa_left);
	draw_text(250, 300, "Der Saboteur zielte mit seiner Waffe auf Andrej, bereit zum Schießen.");
	draw_text(250, 350, "Bevor er den Abzug drücken konnte, wurde er jedoch vom verletzten Dimitri");
	draw_text(250, 400, "angegriffen, der sich an den Saboteur herangeschlichen hatte.");
	draw_text(250, 450, "Die Beiden rangen miteinander um die Waffe, als im Gedränge eine");
	draw_text(250, 500, "Salve von Schüssen abgegeben wurde, die Dimitri durchlöcherte.");
	draw_text(250, 550, "Andrej gelang es, dem Saboteur die Waffe abzunehmen und ihn damit zu erschießen,");
	draw_text(250, 600, "doch Dimitri war bereits an den Folgen seiner Verletzungen dahingeschieden.");
}
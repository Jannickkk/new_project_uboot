// Zeige Namen und Nutzen des Gegenstandes an
if (position_meeting(mouse_x, mouse_y, id))
{
	draw_set_font(fMapExplanation);
	draw_set_color(c_white);
	draw_set_halign(fa_left);
	draw_text(60, 27, string("Verlobungsringe: In diesen Ringen sind die Namen ,Andrej´ und ,Dimitri´ eingraviert. Bevor er sie verloren hat,"));
	draw_text(60, 57, string("scheinen sie sich im Besitz von Dimitri befunden zu haben. Wollte er nach Beendigung der Mission etwa...?"));
	}
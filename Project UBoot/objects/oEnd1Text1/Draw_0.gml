draw_set_font(fGame);
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_text(250, 300, "Andrej erschoss den Saboteur und fand den verwundeten Dimitri. Da Andrej schnell");
draw_text(250, 350, "handelte, konnten Dimitris Wunden noch rechtzeitig versorgt werden.");
draw_text(250, 400, "Die Beiden wurden von ihrer Mannschaft eine Stunde später abgeholt und konnten");
draw_text(250, 450, "somit ihre Mission erfolgreich abschließen.");
draw_text(250, 500, "Um sich von dem Schrecken etwas abzulenken, nahmen sie sich eine Auszeit.");
if global.weddingRingFound = true
{
	draw_text(250, 550, "Dimitri wagte den Schritt und machte Andrej noch auf dem Rückweg einen Antrag.");
}
if global.weddingRingFound = false
{
	draw_text(250, 550, "Durch die Ereignisse sind sie in ihrer Beziehung weiter gewachsen.");
}
// Bestes Ende: Saboteur stirbt,beide Soldaten überleben
if global.foundPassport = true and global.weaponFound = true
{
	room_goto(rEnd1A);
}

// Dimitri opfert sich, Saboteur stirbt
if global.foundPassport = true and global.weaponFound = false
{
	room_goto(rEnd2A);
}

// Dimitri ist schon tot, Andrej erschießt den Saboteur
if global.foundPassport = false and global.weaponFound = true
{
	room_goto(rEnd3A);
}

// Dimitri ist schon tot, der Saboteur bringt Andrej um
if global.foundPassport = false and global.weaponFound = false
{
	room_goto(rEnd4A);
}
// Schleuse
if global.roomNumber = 1
{
	x = 714
	y = 573
}

if global.roomNumber = 2
{
	x = 714
	y = 573
}

// Offiziersmesse
if global.roomNumber = 3
{
	x = 830
	y = 415
}

if global.roomNumber = 4
{
	x = 830
	y = 415
}

if global.roomNumber = 5
{
	x = 830
	y = 415
}

if global.roomNumber = 6
{
	x = 830
	y = 415
}

if global.roomNumber = 7
{
	x = 830
	y = 415
}

if global.roomNumber = 8
{
	x = 830
	y = 415
}

// Brücke
if global.roomNumber = 9
{
	x = 845
	y = 560
}

// Kommandantenkabine
if global.roomNumber = 10
{
	x = 1110
	y = 415
}

// Bugtorpedoraum
if global.roomNumber = 11
{
	x = 1550
	y = 486
}

if global.roomNumber = 12
{
	x = 1550
	y = 486
}

// Mannschaftsquartiere
if global.roomNumber = 13
{
	x = 1638
	y = 486
}

// Kombüse
if global.roomNumber = 28
{
	x = 1135
	y = 560
}

if global.roomNumber = 35
{
	x = 550
	y = 486
}

if global.roomNumber = 36
{
	x = 450
	y = 486
}

// Flur 1A
if global.roomNumber = 37
{
	x = 714
	y = 486
}

// Flur 1B
if global.roomNumber = 38
{
	x = 1000
	y = 486
}

// Flur 3A
if global.roomNumber = 42
{
	x = 1290
	y = 560
}

// Flur 3B
if global.roomNumber = 43
{
	x = 1290
	y = 460
}

// Tür zur Kombüse
if global.roomNumber = 45
{
	x = 1000
	y = 580
}

// Tür zur Brücke
if global.roomNumber = 46
{
	x = 796
	y = 500
}
/// Zeige ein Fragezeichen, um dem Spieler den Weg zu zeigen
// Schließe die Schleusentür auf
if global.doorInteractedAndOpened = false
{
	if global.showUG2 = false
		{
			draw_set_font(fMapExplanation);
			draw_set_color(c_white);
			draw_text(706, 510, "?");
		}
}

// Durchsuche die Räume in der Nähe des Eingangs
if global.doorInteractedAndOpened = true and global.waitingDimitri = false
{
	if global.showUG2 = false
		{
			draw_set_font(fMapExplanation);
			draw_set_color(c_white);
			draw_text(830, 415, "?");
		}
}

// Untersuche die Brücke
if global.waitingDimitri = true and global.foundDestroyedBridge = false
{
	if global.showUG2 = false
		{
			draw_set_font(fMapExplanation);
			draw_set_color(c_white);
			draw_text(845, 560, "?");
		}
}

// Kehre zu Dimitri zurück
if global.foundDestroyedBridge = true and global.lostPartner = false
{
	if global.showUG2 = false
		{
			draw_set_font(fMapExplanation);
			draw_set_color(c_white);
			draw_text(830, 415, "?");
		}
}

// Folge den Verschwundenen
if global.lostPartner = true and global.foundBlood = false
{
	if global.showUG2 = false
		{
			draw_set_font(fMapExplanation);
			draw_set_color(c_white);
			draw_text(1035, 572, "?");
		}
}

// Suche nach dem Code für die verschlossene Tür
if global.foundBlood = true and global.codewordFound = false
{
	if global.showUG2 = true
		{
			draw_set_font(fMapExplanation);
			draw_set_color(c_white);
			draw_text(1060, 400, "?");
		}
}

// Kehre zu der verschlossenen Tür zurück
if global.codewordFound = true and global.door2Unlocked = false
{
	if global.showUG2 = false
		{
			draw_set_font(fMapExplanation);
			draw_set_color(c_white);
			draw_text(1035, 572, "?");
		}
}

// Folge weiter den Verschwundenen
if global.door2Unlocked = true
{
	if global.showUG2 = false
		{
			draw_set_font(fMapExplanation);
			draw_set_color(c_white);
			draw_text(450, 486, "?");
		}
}


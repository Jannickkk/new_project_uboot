// Position des Spielers anzeigen

if global.showUG2 = true
{
	// Mannschaftsquartiere
	if global.roomNumber = 14
	{
		x = 1160
		y = 590
	}

	if global.roomNumber = 15
	{
		x = 1300
		y = 590
	}

	if global.roomNumber = 16
	{
		x = 1300
		y = 513
	}

	if global.roomNumber = 17
	{
		x = 1325
		y = 433
	}

	// Lager des Saboteurs
	if global.roomNumber = 18
	{
		x = 1390
		y = 513
	}

	if global.roomNumber = 19
	{
		x = 1390
		y = 513
	}

	if global.roomNumber = 20
	{
		x = 1390
		y = 582
	}

	if global.roomNumber = 21
	{
		x = 1390
		y = 582
	}

	if global.roomNumber = 22
	{
		x = 1390
		y = 582
	}

	// Tauchkapselraum
	if global.roomNumber = 23
	{
		x = 1450
		y = 582
	}

	if global.roomNumber = 24
	{
		x = 1450
		y = 550
	}

	if global.roomNumber = 25
	{
		x = 1450
		y = 513
	}

	// Maschinenraum A
	if global.roomNumber = 26
	{
		x = 1135
		y = 400
	}

	if global.roomNumber = 27
	{
		x = 1060
		y = 400
	}

	// Maschinenraum B
	if global.roomNumber = 29
	{
		x = 1165
		y = 490
	}

	if global.roomNumber = 30
	{
		x = 1070
		y = 490
	}

	// Hecktorpedoraum
	if global.roomNumber = 31
	{
		x = 990
		y = 490
	}

	if global.roomNumber = 32
	{
		x = 930
		y = 490
	}

	if global.roomNumber = 33
	{
		x = 815
		y = 490
	}

	if global.roomNumber = 34
	{
		x = 670
		y = 490
	}

	// Flur 2A
	if global.roomNumber = 39
	{
		x = 1268
		y = 410
	}

	// Flur 2B
	if global.roomNumber = 40
	{
		x = 1215
		y = 410
	}

	// Flur 2C
	if global.roomNumber = 41
	{
		x = 1180
		y = 420
	}

	// Flur 3C
	if global.roomNumber = 44
	{
		x = 1220
		y = 460
	}
}
else
{
	x = 2000
	y = 2000
}
mydialogue = CreateDialogue();
AddDialogue(mydialogue, "Andrej: Ein Keycard-Leser... scheint gerade erst benutzt worden zu sein.");
AddDialogue(mydialogue, "Andrej: Dabei haben wir doch extra vom Hauptquartier eine Keycard mitbekommen...");
AddDialogue(mydialogue, "Andrej: Hat Dimitri diese Tür etwa mit seiner Keycard geöffnet?");
PlayDialogue(mydialogue);
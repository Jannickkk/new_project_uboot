{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 117,
  "bbox_top": 0,
  "bbox_bottom": 117,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 118,
  "height": 118,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"25f1eaf7-c2d3-4f7a-8e57-175410459195","path":"sprites/sMapClick/sMapClick.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"25f1eaf7-c2d3-4f7a-8e57-175410459195","path":"sprites/sMapClick/sMapClick.yy",},"LayerId":{"name":"c83b609b-9d59-4b6c-a189-79b670dc84e9","path":"sprites/sMapClick/sMapClick.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sMapClick","path":"sprites/sMapClick/sMapClick.yy",},"resourceVersion":"1.0","name":"25f1eaf7-c2d3-4f7a-8e57-175410459195","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sMapClick","path":"sprites/sMapClick/sMapClick.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ee3b5cfc-a430-493d-b3e4-45eac76b2230","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"25f1eaf7-c2d3-4f7a-8e57-175410459195","path":"sprites/sMapClick/sMapClick.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 59,
    "yorigin": 59,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sMapClick","path":"sprites/sMapClick/sMapClick.yy",},
    "resourceVersion": "1.3",
    "name": "sMapClick",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":1.0,"displayName":"default","resourceVersion":"1.0","name":"c83b609b-9d59-4b6c-a189-79b670dc84e9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Icons",
    "path": "folders/Sprites/Icons.yy",
  },
  "resourceVersion": "1.0",
  "name": "sMapClick",
  "tags": [],
  "resourceType": "GMSprite",
}
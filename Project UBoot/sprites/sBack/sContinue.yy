{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 146,
  "bbox_top": 10,
  "bbox_bottom": 147,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 150,
  "height": 150,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"4a5aa5b6-9119-42a8-af19-59a422538317","path":"sprites/sContinue/sContinue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a5aa5b6-9119-42a8-af19-59a422538317","path":"sprites/sContinue/sContinue.yy",},"LayerId":{"name":"726f73eb-015f-47d6-9170-f131324dd61e","path":"sprites/sContinue/sContinue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"4a5aa5b6-9119-42a8-af19-59a422538317","path":"sprites/sContinue/sContinue.yy",},"LayerId":{"name":"d5a82e59-5afc-416c-8068-a4a0e928fdfe","path":"sprites/sContinue/sContinue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sContinue","path":"sprites/sContinue/sContinue.yy",},"resourceVersion":"1.0","name":"4a5aa5b6-9119-42a8-af19-59a422538317","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sContinue","path":"sprites/sContinue/sContinue.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"695ec272-7e9b-4384-a985-8eea9ae5698f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a5aa5b6-9119-42a8-af19-59a422538317","path":"sprites/sContinue/sContinue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 75,
    "yorigin": 75,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sContinue","path":"sprites/sContinue/sContinue.yy",},
    "resourceVersion": "1.3",
    "name": "sContinue",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"726f73eb-015f-47d6-9170-f131324dd61e","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":true,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"d5a82e59-5afc-416c-8068-a4a0e928fdfe","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Extras",
    "path": "folders/Sprites/Extras.yy",
  },
  "resourceVersion": "1.0",
  "name": "sContinue",
  "tags": [],
  "resourceType": "GMSprite",
}
{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 1440,
  "bbox_top": 0,
  "bbox_bottom": 1079,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1441,
  "height": 1080,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"fcbd1ec2-f07f-48df-9a7a-116b8d62144e","path":"sprites/sText3/sText3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fcbd1ec2-f07f-48df-9a7a-116b8d62144e","path":"sprites/sText3/sText3.yy",},"LayerId":{"name":"17218efd-b944-490c-a4de-c48d1a8f72f4","path":"sprites/sText3/sText3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sText3","path":"sprites/sText3/sText3.yy",},"resourceVersion":"1.0","name":"fcbd1ec2-f07f-48df-9a7a-116b8d62144e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sText3","path":"sprites/sText3/sText3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ba541af5-5695-46e8-9c9f-4a839e08a69c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fcbd1ec2-f07f-48df-9a7a-116b8d62144e","path":"sprites/sText3/sText3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 720,
    "yorigin": 540,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sText3","path":"sprites/sText3/sText3.yy",},
    "resourceVersion": "1.3",
    "name": "sText3",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"17218efd-b944-490c-a4de-c48d1a8f72f4","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Texts",
    "path": "folders/Sprites/Texts.yy",
  },
  "resourceVersion": "1.0",
  "name": "sText3",
  "tags": [],
  "resourceType": "GMSprite",
}
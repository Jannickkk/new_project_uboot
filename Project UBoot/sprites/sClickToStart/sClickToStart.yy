{
  "bboxMode": 1,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 699,
  "bbox_top": 0,
  "bbox_bottom": 99,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 700,
  "height": 100,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"b3715a65-d453-4aa5-ae45-fec3b3421d19","path":"sprites/sClickToStart/sClickToStart.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b3715a65-d453-4aa5-ae45-fec3b3421d19","path":"sprites/sClickToStart/sClickToStart.yy",},"LayerId":{"name":"cdef7131-1a17-4dbd-987b-2e5a6f0a4ee3","path":"sprites/sClickToStart/sClickToStart.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sClickToStart","path":"sprites/sClickToStart/sClickToStart.yy",},"resourceVersion":"1.0","name":"b3715a65-d453-4aa5-ae45-fec3b3421d19","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sClickToStart","path":"sprites/sClickToStart/sClickToStart.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"99628df8-4f82-4846-a3f1-08e7b874663a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b3715a65-d453-4aa5-ae45-fec3b3421d19","path":"sprites/sClickToStart/sClickToStart.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 350,
    "yorigin": 50,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sClickToStart","path":"sprites/sClickToStart/sClickToStart.yy",},
    "resourceVersion": "1.3",
    "name": "sClickToStart",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":60.0,"displayName":"default","resourceVersion":"1.0","name":"cdef7131-1a17-4dbd-987b-2e5a6f0a4ee3","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Titlescreen",
    "path": "folders/Sprites/Titlescreen.yy",
  },
  "resourceVersion": "1.0",
  "name": "sClickToStart",
  "tags": [],
  "resourceType": "GMSprite",
}